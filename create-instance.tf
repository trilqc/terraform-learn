# create-instance.tf
 
resource "aws_instance" "instance1" {
  ami                         = var.instance_ami
  availability_zone           = var.aws_region_az[0]
  instance_type               = var.instance_type
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.sg.id]
  subnet_id                   = aws_subnet.subnetaz1.id
  key_name                    = var.key_pair
 
  root_block_device {
    delete_on_termination = true
    encrypted             = false
    volume_size           = var.root_device_size
    volume_type           = var.root_device_type
  }
 
  tags = {
    "Owner"               = var.owner
    "Name"                = "${var.owner}-instance1"
    "KeepInstanceRunning" = "false"
  }
}
resource "aws_instance" "instance2" {
  ami                         = var.instance_ami
  availability_zone           = var.aws_region_az[0]
  instance_type               = var.instance_type
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.sg.id]
  subnet_id                   = aws_subnet.subnetaz1.id
  key_name                    = var.key_pair_2
 
  root_block_device {
    delete_on_termination = true
    encrypted             = false
    volume_size           = var.root_device_size
    volume_type           = var.root_device_type
  }
 
  tags = {
    "Owner"               = var.owner
    "Name"                = "${var.owner}-instance2"
    "KeepInstanceRunning" = "false"
  }
}
resource "aws_instance" "instance3" {
  ami                         = var.instance_ami
  availability_zone           = var.aws_region_az[1]
  instance_type               = var.instance_type
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.sg.id]
  subnet_id                   = aws_subnet.subnetaz2.id 
  key_name                    = var.key_pair_3
 
  root_block_device {
    delete_on_termination = true
    encrypted             = false
    volume_size           = var.root_device_size
    volume_type           = var.root_device_type
  }
 
  tags = {
    "Owner"               = var.owner
    "Name"                = "${var.owner}-instance3"
    "KeepInstanceRunning" = "false"
  }
}
