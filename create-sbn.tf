#create-sbn.tf
 
resource "aws_subnet" "subnetaz1" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.sbn_cidr_block_az1
  map_public_ip_on_launch = var.sbn_public_ip
  availability_zone       = var.aws_region_az[0]
 
  tags = {
    "Owner" = var.owner
    "Name"  = "${var.owner}-subnetaz1"
  }
}
resource "aws_subnet" "subnetaz2" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.sbn_cidr_block_az2
  map_public_ip_on_launch = var.sbn_public_ip
  availability_zone       = var.aws_region_az[1]
 
  tags = {
    "Owner" = var.owner
    "Name"  = "${var.owner}-subnetaz2"
  }
}
