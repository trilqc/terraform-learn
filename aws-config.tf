# aws_config.tf
 
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.26"
    }
  }
}
 
provider "aws" {
#  profile = "default"
  region  = var.aws_region
  access_key = var.access_key
  secret_key = var.secret_key
  token      = var.session_token
}
