# variable_output.tf
 
# Variables to show after the deployment
#########################################
 
output "public_ip1" {
  value = aws_instance.instance1.public_ip    
}
output "public_ip2" {
  value = aws_instance.instance2.public_ip    
}
output "public_ip3" {
  value = aws_instance.instance3.public_ip    
}